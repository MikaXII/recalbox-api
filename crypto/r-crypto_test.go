package rcrypto

import (
	"bytes"
	"crypto/md5"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckIfHashed(t *testing.T) {
	hashed := "enc:test"
	unhashed := "test"
	assert.True(t, CheckIfHashed(hashed))
	assert.False(t, CheckIfHashed(unhashed))
}

func TestHashString(t *testing.T) {
	superpass := "recaluser"
	supersalt := []byte("aa-bb-cc-dd")
	HashString(&superpass, supersalt)
	assert.Equal(t, "enc:f86cccbb3955625a3a0d603529a091aca424cab0aba32e9afe0137b024d99de3", superpass)
}

func TestGetSumHash(t *testing.T) {
	assert.Equal(t, "f71dbe52628a3f83a77ab494817525c6", GetSumHash(md5.New(), bytes.NewBuffer([]byte("toto"))))
}
