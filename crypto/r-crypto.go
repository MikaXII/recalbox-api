package rcrypto

import (
	"crypto"
	"fmt"
	"hash"
	"io"
	"strings"
)

func CheckIfHashed(pass string) bool {
	return strings.HasPrefix(pass, "enc:")
}

func HashString(pass *string, salt []byte) {
	sha := crypto.SHA256.New()
	saltPass := fmt.Sprintf("%s%s", salt, *pass)
	sha.Write([]byte(saltPass))
	*pass = "enc:" + fmt.Sprintf("%x", sha.Sum(nil))
}

// GetSumHash Get hash string of a file
func GetSumHash(h hash.Hash, reader io.Reader) string {
	io.Copy(h, reader)
	return fmt.Sprintf("%x", h.Sum(nil))
}
