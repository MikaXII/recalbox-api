package es

import (
	"bytes"
	"os/exec"
	"path/filepath"
	"regexp"
)

type EsInterface interface {
	SetGame(ps string) (string, bool)
	PsEsGame() (string, error)
}

type Es struct{}

func (es *Es) PsEsGame() (string, error) {
	cmd := exec.Command("ps | grep emulatorlauncher.py | grep -v 'c python' | grep -v grep")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", nil
	}
	defer stdout.Close()
	cmd.Start()
	buf := new(bytes.Buffer)
	buf.ReadFrom(stdout)
	cmd.Wait()
	return buf.String(), err
}

func (es *Es) SetGame(ps string) (string, bool) {
	return "", false
}

func GetCurrentGame(esInterface *EsInterface) (string, bool) {
	commandResult, _ := (*esInterface).PsEsGame()
	// commandResult := `1448 root  python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc -p1index 0 -p1guid 030000006d04000018c2000010010000 -p1name Logitech Logitech RumblePad 2 USB -p1nbaxes 4 -p1devicepath /dev/input/event0 -system nes -rom /recalbox/share/roms/nes/Super Mario Bros. (Japan, USA).nes -emulator default -core default -ratio auto`
	regex, _ := regexp.Compile(`-system ([\W\w]*) -rom ([\W\w]*) -emulator`)
	result_slice := regex.FindStringSubmatch(commandResult)
	//esInterface.System = result_slice[1]
	basename := filepath.Base(result_slice[2])
	//esInterface.Game = strings.TrimSuffix(basename, filepath.Ext(basename))
	(*esInterface).SetGame(commandResult)
	return basename, true
}
