package es

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// # ps | grep emulatorlauncher.py | grep -v 'c python' | grep -v grep
//  1448 root     python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc -p1index 0 -p1guid 030000006d04000018c2000010010000 -p1name Logitech Logitech RumblePad 2 USB -p1nbaxes 4 -p1devicepath /dev/input/event0 -system nes -rom /recalbox/share/roms/nes/Super Mario Bros. (Japan, USA).nes -emulator default -core default -ratio auto
type FakeSys struct{}

type FakeEs struct {
	Game     string
	Launched bool
	System   string
}

func (fakeEs *FakeEs) SetGame(ps string) (string, bool) {
	if !fakeEs.Launched {
		return "", false
	}

	return fakeEs.Game, fakeEs.Launched
}

func (fakeEs *FakeEs) PsEsGame() (string, error) {
	return `1448 root  python /usr/lib/python2.7/site-packages/configgen/emulatorlauncher.pyc -p1index 0 -p1guid 030000006d04000018c2000010010000 -p1name Logitech Logitech RumblePad 2 USB -p1nbaxes 4 -p1devicepath /dev/input/event0 -system nes -rom /recalbox/share/roms/nes/Super Mario Bros. (Japan, USA).nes -emulator default -core default -ratio auto`, nil
}

func TestGetCurrentGameReturnWormsTrue(t *testing.T) {
	esManager := FakeEs{Game: "Super Mario Bros. (Japan, USA)", Launched: true}
	esInterface := new(EsInterface)
	*esInterface = &esManager
	name, launched := GetCurrentGame(esInterface)
	assert.Equal(t, "Super Mario Bros. (Japan, USA).nes", name)
	assert.True(t, launched)
}

// func TestGetCurrentGameReturnEmptyFalse(t *testing.T) {
// 	esManager := FakeEs{Game: "", Launched: false}
// 	esInterface := new(EsInterface)
// 	*esInterface = &esManager
// 	name, launched := GetCurrentGame(esInterface)
// 	assert.Empty(t, name)
// 	assert.False(t, launched)
// }

func TestPsEsGameJustWork(t *testing.T) {
	es := Es{}
	_, err := es.PsEsGame()
	assert.Empty(t, err)
}
