package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/gin-gonic/contrib/jwt"
	"github.com/gin-gonic/gin"
	"gitlab.com/MikaXII/recalbox-api/api"
	"gitlab.com/MikaXII/recalbox-api/crypto"
)

func main() {
	err := InitRouter().Run(":8080")
	if err != nil {
		log.Fatalf("%s", err)
	}
}

// InitRouter init of endpoint configuration...
func InitRouter() *gin.Engine {
	r := gin.Default()

	c := new(api.RecalboxFS)
	c.Init(os.Getenv("CONFIG_PATH"))

	b, _ := ioutil.ReadFile(os.Getenv("UUID_PATH"))
	if !rcrypto.CheckIfHashed(c.Config.Auth.Pass) {
		rcrypto.HashString(&c.Config.Auth.Pass, b)
		api.UpdatePass(c, c.Config)
	}

	r.POST("/login", api.RecalAuth(c.Config))

	apiV1 := r.Group("/v1") // apiV1 := r.Group("/v1", gin.BasicAuth(gin.Accounts{c.Config.Auth.User: c.Config.Auth.Pass}))
	apiV1.Use(jwt.Auth(fmt.Sprintf("%s", b)))

	GroupV1(apiV1, c)

	r.GET("/", getAllEndpoint(r))

	return r
}

// Print all available endpoint
func getAllEndpoint(r *gin.Engine) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		c.JSON(200, r.Routes())
	}
	return gin.HandlerFunc(fn)
}

func GroupV1(apiV1 *gin.RouterGroup, c *api.RecalboxFS) {
	apiV1.GET("/systems", api.ShowAllSystem(c))
	apiV1.GET("/systems/roms", api.ShowAllSystemsAndRoms(c))
	apiV1.GET("/system/:id/roms", api.ShowAllMediasInSystem(c))
	apiV1.GET("/system/:id/roms/hash", api.ShowAllMediasInSystemWithHash(c))
	apiV1.GET("/system/:id/rom/:mediaName", api.ShowMediaInfo(c, c.Config.SystemsPath))

	apiV1.GET("/system/:id/rom/:mediaName/download", api.DownloadMedia(c, c.Config.SystemsPath))
	apiV1.POST("/system/:id", api.UploadMedias(c.Config.SystemsPath))
	apiV1.DELETE("/system/:id/rom/:mediaName", api.DeleteMedia(c, c.Config.SystemsPath))

	apiV1.GET("/bios", api.ShowAllMediasInBios(c))
	apiV1.GET("/bios/:mediaName", api.ShowMediaInfo(c, c.Config.BiosPath))
	apiV1.GET("/bios/:mediaName/download", api.DownloadMedia(c, c.Config.BiosPath))
	apiV1.POST("/bios", api.UploadMedias(c.Config.BiosPath))
	apiV1.DELETE("/bios/:romName", api.DeleteMedia(c, c.Config.BiosPath))
}
