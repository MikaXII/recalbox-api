#!/bin/bash
PKG_LIST=$(go list ./... | grep -v /vendor/)
BASEDIR=$(pwd)
export UUID_PATH=$BASEDIR/resources/uuid
export CONFIG_PATH_TEST=/recalbox/share/system/configs/api-config.json
export CONFIG_PATH=$BASEDIR/resources/api-config.json
for package in ${PKG_LIST}; do
   go test -covermode=count -coverprofile "cover/${package##*/}.cov" "$package";
done
echo "mode: count" > coverage.cov
for file in cover/*; do 
    awk NR\>1 $file >> coverage.cov 
done

#tail -q -n +2 cover/*.cov >> cover/coverage.cov 
go tool cover -func=coverage.cov || exit 0
#go test ${PKG_LIST} -v -coverprofile .testCoverage.txt