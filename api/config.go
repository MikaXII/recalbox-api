package api

// Config structure for api Init
type Config struct {
	Version     string `json:"version"`
	SystemsPath string `json:"systemsPath"`
	BiosPath    string `json:"biosPath"`
	Auth        Auth   `json:"auth"`
}
type Auth struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}
