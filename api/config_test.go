package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadConfig(t *testing.T) {
	tFs := TestFilesytem{}
	tFs.Init("/recalbox/share/system/configs/api-config.json")
	assert.NotEmpty(t, tFs.Config.Version)
	assert.NotEmpty(t, tFs.Config.SystemsPath)
	assert.NotEmpty(t, tFs.Config.BiosPath)
	assert.Equal(t, "recaluser", tFs.Config.Auth.User)
	assert.Equal(t, "recalboxrox", tFs.Config.Auth.Pass)
}

func TestLoadConfigWithBadPath(t *testing.T) {
	tFs := TestFilesytem{}
	err := tFs.Init("/recalbox/share/system/configs/api-config2.json")
	assert.Nil(t, tFs.Config)
	assert.NotNil(t, err)
}
