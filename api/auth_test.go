package api

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/MikaXII/recalbox-api/crypto"
)

func TestRecalAuth(t *testing.T) {
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")

	b, _ := ioutil.ReadFile(os.Getenv("UUID_PATH"))
	if !rcrypto.CheckIfHashed(testFs.Config.Auth.Pass) {
		rcrypto.HashString(&testFs.Config.Auth.Pass, b)
		UpdatePass(&testFs, testFs.Config)
	}

	r.POST("/login", RecalAuth(testFs.Config))
	json := []byte(`{"user":"recaluser","pass":"recalboxrox"}`)
	req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(json))
	req.Header.Set("Content-Type", "application/json")
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 200, resp.Code)
}
func TestRecalAuthBadCred(t *testing.T) {
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")

	b, _ := ioutil.ReadFile(os.Getenv("UUID_PATH"))
	if !rcrypto.CheckIfHashed(testFs.Config.Auth.Pass) {
		rcrypto.HashString(&testFs.Config.Auth.Pass, b)
		UpdatePass(&testFs, testFs.Config)
	}

	r.POST("/login", RecalAuth(testFs.Config))
	json := []byte(`{"user":"recaluser","pass":"recalboxroxBad"}`)
	req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(json))
	req.Header.Set("Content-Type", "application/json")
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 400, resp.Code)
}
