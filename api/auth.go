package api

import (
	"io/ioutil"
	"time"

	"os"

	jwt_lib "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/json"
	"gitlab.com/MikaXII/recalbox-api/crypto"
)

func RecalAuth(config *Config) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		b, _ := ioutil.ReadFile(os.Getenv("UUID_PATH"))

		var auth Auth
		c.BindJSON(&auth)
		rcrypto.HashString(&auth.Pass, b)

		if config.Auth != auth {
			c.JSON(400, gin.H{"message": "BAD username/password"})
			return
		}

		token := jwt_lib.New(jwt_lib.GetSigningMethod("HS256"))
		// Set some claims
		token.Claims = jwt_lib.MapClaims{
			"Id":  "Recalbox",
			"exp": time.Now().Add(time.Hour * 12).Unix(),
		}
		// Sign and get the complete encoded token as a string
		tokenString, err := token.SignedString(b)
		if err != nil {
			c.JSON(500, gin.H{"message": "Could not generate token"})
		} else {
			c.JSON(200, gin.H{"token": tokenString})
		}
	}
	return fn
}

func UpdatePass(rfs FileSystemInterface, c *Config) {
	cJson, _ := json.Marshal(c)
	rfs.WriteFile(os.Getenv("CONFIG_PATH"), cJson, 0644)
	//ioutil.WriteFile(os.Getenv("CONFIG_PATH"), cJson, 0644)
}
