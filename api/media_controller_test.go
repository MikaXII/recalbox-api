package api

import (
	"bytes"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestShowAllSystemInfo(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/systems", ShowAllSystem(&testFs))
	req, _ := http.NewRequest("GET", "/systems", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, resp.Code, http.StatusOK)
	assert.NotEmpty(t, resp.Body)
}

func TestShowAllRomsInSystem(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/system/:id/roms", ShowAllMediasInSystem(&testFs))
	req, _ := http.NewRequest("GET", "/system/nes/roms", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, resp.Code, http.StatusOK)
	assert.NotEmpty(t, resp.Body)
}

func TestShowAllSystemsAndRoms(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/systems/roms", ShowAllSystemsAndRoms(&testFs))
	req, _ := http.NewRequest("GET", "/systems/roms", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, resp.Code, http.StatusOK)
	assert.NotEmpty(t, resp.Body)
}

func TestShowAllBiosInfo(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/bios", ShowAllMediasInBios(&testFs))
	req, _ := http.NewRequest("GET", "/bios", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

//"/system/:id/rom/:mediaName
func TestShowMediaInfo(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/system/:id/rom/:mediaName", ShowMediaInfo(&testFs, testFs.BaseRomPath()))
	req, _ := http.NewRequest("GET", "/system/nes/rom/file1", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

func TestShowMediaInfoBadFile(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/system/:id/rom/:mediaName", ShowMediaInfo(&testFs, testFs.BaseRomPath()))
	req, _ := http.NewRequest("GET", "/system/nes/rom/unknowfile", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 404, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

func TestDownloadMedia(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/system/:id/rom/:mediaName/download", DownloadMedia(&testFs, testFs.BaseRomPath()))
	req, _ := http.NewRequest("GET", "/system/nes/rom/file2/download", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 404, resp.Code) // Unable to download on memFs
	assert.NotEmpty(t, resp.Body)
}

func TestDownloadMediaBadFile(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/system/:id/rom/:mediaName/download", DownloadMedia(&testFs, testFs.BaseRomPath()))
	req, _ := http.NewRequest("GET", "/system/nes/rom/fileUnknow/download", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 404, resp.Code) // Unable to download on memFs
	assert.NotEmpty(t, resp.Body)
}

func TestDeleteMedia(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.DELETE("/system/:id/rom/:mediaName", DeleteMedia(&testFs, testFs.BaseRomPath()))
	req, _ := http.NewRequest("DELETE", "/system/nes/rom/file1", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

func TestDeleteMediaBadFile(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.DELETE("/system/:id/rom/:mediaName", DeleteMedia(&testFs, testFs.BaseRomPath()))
	req, _ := http.NewRequest("DELETE", "/system/nes/rom/unknowfile", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, 404, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

func TestUploadMedias(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.POST("/system/:id", UploadMedias(testFs.Config.SystemsPath))
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	w.CreateFormFile("roms[]", "test")

	req, _ := http.NewRequest("POST", "/system/nes", &b)
	req.Header.Set("Content-Type", w.FormDataContentType())
	w.Close()
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	os.Remove("test")
	assert.Equal(t, 200, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

func TestShowAllRomsInSystemWithHash(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	testFs := TestFilesytem{}
	testFs.Init("/recalbox/share/system/configs/api-config.json")
	r.GET("/system/:id/roms/hash", ShowAllMediasInSystemWithHash(&testFs))
	req, _ := http.NewRequest("GET", "/system/nes/roms/hash", nil)
	resp := httptest.NewRecorder()
	r.ServeHTTP(resp, req)
	assert.Equal(t, http.StatusOK, resp.Code)
	assert.NotEmpty(t, resp.Body)
}

// func TestUploadMediasBad(t *testing.T) {
// 	gin.SetMode(gin.TestMode)
// 	r := gin.Default()
// 	testFs := TestFilesytem{}
// 	testFs.Init("/recalbox/share/system/configs/api-config.json")
// 	r.POST("/system/:id", UploadMedias(testFs.Config.SystemsPath))
// 	var b bytes.Buffer
// 	w := multipart.NewWriter(&b)
// 	w.CreateFormFile("rom", "test")

// 	req, _ := http.NewRequest("POST", "/system/nes", &b)
// 	req.Header.Set("Content-Type", w.FormDataContentType())
// 	w.Close()
// 	resp := httptest.NewRecorder()
// 	r.ServeHTTP(resp, req)
// 	assert.Equal(t, 503, resp.Code)
// 	assert.NotEmpty(t, resp.Body)
// }
