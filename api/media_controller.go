package api

import (
	"fmt"
	"os"
	"runtime"

	"github.com/gin-gonic/gin"
)

// ShowAllSystem Show all systems in recalbox FS
func ShowAllSystem(rfs FileSystemInterface) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		files, _ := rfs.ReadDirSystemPath("")
		sysList := []string{}
		for _, item := range files {
			sysList = append(sysList, item.Name())
		}
		c.JSON(200, sysList)
	}
	return gin.HandlerFunc(fn)
}

func ShowAllSystemsAndRoms(rfs FileSystemInterface) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		rbSys := []RBSystem{}
		files, _ := rfs.ReadDirSystemPath("")
		for _, item := range files {
			if item.IsDir() {
				rb := RBSystem{}
				rb.Name = item.Name()
				roms := []Media{}
				rFiles, _ := rfs.ReadDirSystemPath(item.Name() + "/")
				for _, rom := range rFiles {
					if !rom.IsDir() {
						//roms = append(roms, Media{Filename: rfs.BaseRomPath() + item.Name() + "/" + rom.Name(), Name: rom.Name()})
						roms = append(roms, *NewMedia(rfs.BaseRomPath()+item.Name()+"/"+rom.Name(), rom.Name()))
					}
				}
				rb.Roms = roms
				rbSys = append(rbSys, rb)
			}
		}
		c.JSON(200, rbSys)
	}
	return gin.HandlerFunc(fn)
}

// ShowAllRomsInSystem Show all roms in a system directory
func ShowAllMediasInSystem(rfs FileSystemInterface) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		system := c.Param("id")
		roms := []Media{}
		filePath := system
		files, _ := rfs.ReadDirSystemPath(filePath)
		for _, item := range files {
			if !item.IsDir() {
				roms = append(roms, Media{Filename: rfs.BaseRomPath() + filePath + "/" + item.Name(), Name: item.Name()})
			}
		}
		c.JSON(200, roms)
	}
	return gin.HandlerFunc(fn)
}

// ShowAllRomsInSystem Show all roms in a system directory
func ShowAllMediasInBios(rfs FileSystemInterface) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		bios := []Media{}
		files, _ := rfs.ReadDirBiosPath()
		for _, item := range files {
			if !item.IsDir() {
				//bios = append(bios, Media{Filename: rfs.BaseBiosPath() + item.Name(), Name: item.Name()})
				media := NewMedia(rfs.BaseBiosPath()+item.Name(), item.Name())
				media.GetHash(rfs)
				bios = append(bios, *media)

			}
		}
		c.JSON(200, bios)
	}
	return gin.HandlerFunc(fn)
}

func ShowMediaInfo(rfs FileSystemInterface, basepath string) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		system := c.Param("id") + "/"
		romName := c.Param("mediaName")
		filePath := basepath + system + romName
		if _, err := rfs.Stat(filePath); err == nil {
			media := NewMedia(filePath, romName)
			media.GetHash(rfs)
			c.JSON(200, media)
		} else {
			c.JSON(404, err.Error())
		}
	}
	return gin.HandlerFunc(fn)
}

func DownloadMedia(rfs FileSystemInterface, basepath string) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		system := c.Param("id") + "/"
		mediaName := c.Param("mediaName")
		filePath := basepath + system + mediaName
		if _, err := rfs.Stat(filePath); err == nil {
			c.Writer.Header().Add("Content-Disposition", "attachment; filename="+mediaName)
			c.File(filePath)
		} else {
			c.JSON(404, err.Error())
		}

	}
	return gin.HandlerFunc(fn)
}

func UploadMedias(basepath string) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		system := c.Param("id") + "/"
		filePath := basepath + system
		form, _ := c.MultipartForm()
		files := form.File["roms[]"]

		for _, file := range files {
			// Upload the file to specific dst.
			if err := c.SaveUploadedFile(file, file.Filename); err != nil {
				c.String(405, fmt.Sprintf("upload file err: %s", err.Error()))
				return
			}
			c.SaveUploadedFile(file, filePath+file.Filename)
		}
		c.JSON(200, fmt.Sprintf("%d files uploaded!", len(files)))

	}
	return gin.HandlerFunc(fn)
}

func DeleteMedia(rfs FileSystemInterface, basepath string) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		system := c.Param("id") + "/"
		romName := c.Param("mediaName")
		filePath := basepath + system + romName
		if _, err := rfs.Stat(filePath); err == nil {
			os.Remove(filePath)
			c.JSON(200, fmt.Sprintf("File %s deleted", filePath))
		} else {
			c.JSON(404, err.Error())
		}

	}
	return gin.HandlerFunc(fn)
}

func ShowAllMediasInSystemWithHash(rfs FileSystemInterface) gin.HandlerFunc {
	fn := func(c *gin.Context) {
		system := c.Param("id")
		roms := []*Media{}
		filePath := system
		files, _ := rfs.ReadDirSystemPath(filePath)
		for _, item := range files {
			if !item.IsDir() {
				rom := &Media{Filename: rfs.BaseRomPath() + filePath + "/" + item.Name(), Name: item.Name()}
				rom.GetHash(rfs)
				roms = append(roms, rom)

			}
		}
		c.JSON(200, roms)
		roms = nil
		if gin.Mode() != gin.ReleaseMode {
			PrintMemUsage()
		}
		// Force GC to clear up, should see a memory drop
		//	runtime.GC()
		//PrintMemUsage()
	}
	return gin.HandlerFunc(fn)
}

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tHeapAlloc = %v MiB", bToMb(m.HeapAlloc))
	fmt.Printf("\tNumGC = %v\n", m.NumGC)

}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
