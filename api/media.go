package api

import (
	"archive/zip"
	"crypto/md5"
	"crypto/sha256"
	"encoding/json"
	"hash/crc32"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"gitlab.com/MikaXII/recalbox-api/crypto"
)

// RecalFs interface for recalbox Filesystem
type FileSystemInterface interface {
	BaseRomPath() string
	BaseBiosPath() string
	ReadDirSystemPath(filePath string) ([]os.FileInfo, error)
	ReadDirBiosPath() ([]os.FileInfo, error)
	ReadFile(filePath string) ([]byte, error)
	Init(filePath string) error
	Stat(filePath string) (os.FileInfo, error)
	WriteFile(path string, data []byte, right os.FileMode)
	Open(filepath string) (io.Reader, error)
}

// CustomFS is use for prod (RecalFS)
type RecalboxFS struct {
	Config *Config
}

// Rom struct represent a rom file with info
type Media struct {
	Name     string
	Filename string
	Hash     map[string]string `json:",omitempty"`
}

type RBSystem struct {
	Name string
	Roms []Media
}

// ReadDir Read a directory
func (recalboxFS *RecalboxFS) ReadDirSystemPath(filePath string) ([]os.FileInfo, error) {
	return ioutil.ReadDir(recalboxFS.Config.SystemsPath + filePath)
}

func (recalboxFS *RecalboxFS) ReadDirBiosPath() ([]os.FileInfo, error) {
	return ioutil.ReadDir(recalboxFS.Config.BiosPath)
}

func (recalboxFS *RecalboxFS) BaseRomPath() string {
	return recalboxFS.Config.SystemsPath
}

func (recalboxFS *RecalboxFS) BaseBiosPath() string {
	return recalboxFS.Config.BiosPath
}

func (recalboxFS *RecalboxFS) Stat(filepath string) (os.FileInfo, error) {
	return os.Stat(filepath)
}

// ReadFile Read a specific file
func (recalboxFS *RecalboxFS) ReadFile(filePath string) ([]byte, error) {
	return ioutil.ReadFile(filePath)
}

func (recalboxFS *RecalboxFS) Init(configFile string) error {
	b, err := recalboxFS.ReadFile(configFile)
	if err != nil {
		return err
	}
	json.Unmarshal(b, &recalboxFS.Config)
	return nil
}
func (recalboxFS *RecalboxFS) Open(filepath string) (io.Reader, error) {
	return os.Open(filepath)
}

func (media *Media) GetHash(rfs FileSystemInterface) {

	f, _ := rfs.Open(media.Filename)
	f1, _ := rfs.Open(media.Filename)
	f2, _ := rfs.Open(media.Filename)

	media.Hash = make(map[string]string)
	media.Hash["md5"] = rcrypto.GetSumHash(md5.New(), f)        //bytes.NewReader(buf.Bytes()))       //append(media.Hash, rcrypto.GetSumHash(md5.New(), bytes.NewReader(buf.Bytes())))
	media.Hash["crc"] = rcrypto.GetSumHash(crc32.NewIEEE(), f1) //bytes.NewReader(buf.Bytes())) //media.Hash = append(media.Hash, rcrypto.GetSumHash(crc32.NewIEEE(), bytes.NewReader(buf.Bytes())))
	media.Hash["sha256"] = rcrypto.GetSumHash(sha256.New(), f2) //bytes.NewReader(buf.Bytes())) // media.Hash = append(media.Hash, rcrypto.GetSumHash(sha256.New(), bytes.NewReader(buf.Bytes())))

	if filepath.Ext(media.Filename) == ".zip" {
		zrc, err := zip.OpenReader(media.Filename)
		if err == nil {
			defer zrc.Close()
			for k, v := range zrc.File {
				crc32InString := strconv.FormatUint(uint64(v.CRC32), 16)
				//	println(item.Name(), crc32InString)
				media.Hash["crcOrigin-"+strconv.Itoa(k)] = crc32InString
			}
		}
	}
}

func NewMedia(filepath string, filename string) *Media {
	media := &Media{Filename: filepath, Name: filename}
	return media
}

func (recalboxFS *RecalboxFS) WriteFile(path string, data []byte, right os.FileMode) {
	ioutil.WriteFile(path, data, right)
}
