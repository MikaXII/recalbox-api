package api

import (
	"bytes"
	"crypto/md5"
	"crypto/sha1"
	"encoding/json"
	"hash/crc32"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/MikaXII/recalbox-api/crypto"
)

type TestFilesytem struct {
	Config *Config
}

func MockedRecalboxFSOverlay() afero.Fs {
	bzip, _ := ioutil.ReadFile("./resources/test.zip")
	appFS := afero.NewMemMapFs()
	appFS.MkdirAll("/recalbox/share/system/configs", 0755)
	appFS.MkdirAll("/recalbox/share/roms", 0755)
	appFS.MkdirAll("/recalbox/share/roms/nes", 0755)
	appFS.MkdirAll("/recalbox/share/bios", 0755)

	afero.WriteFile(appFS, "/recalbox/share/bios/bios1", []byte("file a"), 0644)
	afero.WriteFile(appFS, "/recalbox/share/roms/nes/file1", []byte("file b"), 0644)
	afero.WriteFile(appFS, "/recalbox/share/roms/nes/file2", []byte("file c"), 0644)
	afero.WriteFile(appFS, "/recalbox/share/roms/nes/test.zip", bzip, 0644)
	afero.WriteFile(appFS, "/recalbox/share/system/configs/api-config.json", []byte(`{
  "version": "0.0.1",
  "systemsPath": "/recalbox/share/roms/",
  "biosPath": "/recalbox/share/bios/",
  "auth": {
    "user": "recaluser",
    "pass": "recalboxrox"
  }
}`), 0644)
	return appFS
}

func TestBaseRomPath(t *testing.T) {
	cFs := TestFilesytem{}
	cFs.Init(os.Getenv("CONFIG_PATH"))
	path := cFs.BaseRomPath()
	assert.NotNil(t, path)
}
func TestBaseBiosPath(t *testing.T) {
	cFs := TestFilesytem{}
	cFs.Init(os.Getenv("CONFIG_PATH"))
	path := cFs.BaseBiosPath()
	assert.NotNil(t, path)
}
func TestInit(t *testing.T) {
	cFs := RecalboxFS{}
	err := cFs.Init(os.Getenv("CONFIG_PATH"))
	assert.Nil(t, err)
}

func TestInitWithBadPath(t *testing.T) {
	cFs := RecalboxFS{}
	err := cFs.Init(os.Getenv("/dev/null"))
	assert.NotNil(t, err)
}
func TestNewMedia(t *testing.T) {
	cFs := RecalboxFS{}
	cFs.Init(os.Getenv("CONFIG_PATH"))
	media := NewMedia(os.Getenv("CONFIG_PATH"), "api.json")
	assert.NotNil(t, media)
}
func TestCustomFS_ReadDir(t *testing.T) {
	cFs := RecalboxFS{}
	cFs.Init(os.Getenv("CONFIG_PATH"))
	_, err := cFs.ReadDirSystemPath("")
	assert.Nil(t, err)
}

func TestCustomFS_ReadBios(t *testing.T) {
	cFs := RecalboxFS{}
	cFs.Init(os.Getenv("CONFIG_PATH"))
	_, err := cFs.ReadDirBiosPath()
	assert.Nil(t, err)
}

func TestCustomFS_ReadFile(t *testing.T) {
	cFs := RecalboxFS{}
	cFs.Init(os.Getenv("CONFIG_PATH"))
	_, err := cFs.ReadFile("/dev/null")
	assert.Nil(t, err)
}

func (tFs *TestFilesytem) ReadDirSystemPath(filePath string) ([]os.FileInfo, error) {
	path := tFs.Config.SystemsPath + filePath
	appFs := MockedRecalboxFSOverlay()
	return afero.ReadDir(appFs, path)
}

func (tFs *TestFilesytem) ReadDirBiosPath() ([]os.FileInfo, error) {
	appFS := MockedRecalboxFSOverlay()
	return afero.ReadDir(appFS, tFs.Config.BiosPath)
}

func (tFs *TestFilesytem) ReadFile(filePath string) ([]byte, error) {
	appFS := MockedRecalboxFSOverlay()
	return afero.ReadFile(appFS, filePath)
}

func (tFs *TestFilesytem) Init(configFile string) error {
	b, err := tFs.ReadFile(configFile)
	if err != nil {
		return err
	}

	json.Unmarshal(b, &tFs.Config)

	return nil
}

func (tFs *TestFilesytem) BaseRomPath() string {
	return tFs.Config.SystemsPath
}

func (tFs *TestFilesytem) BaseBiosPath() string {
	return tFs.Config.BiosPath
}

func (tFs *TestFilesytem) Stat(filePath string) (os.FileInfo, error) {
	appFS := MockedRecalboxFSOverlay()
	return afero.Fs.Stat(appFS, filePath)
}

func (tFs *TestFilesytem) WriteFile(path string, data []byte, right os.FileMode) {
	appFs := MockedRecalboxFSOverlay()
	afero.WriteFile(appFs, path, data, right)
}

func (tFs *TestFilesytem) Open(filepath string) (io.Reader, error) {
	appFs := MockedRecalboxFSOverlay()
	return afero.Fs.Open(appFs, filepath)
}

func TestGetAllHash(t *testing.T) {
	appFS := MockedRecalboxFSOverlay()

	f, _ := appFS.Open("/recalbox/share/roms/nes/file1")
	defer f.Close()
	var buf1 bytes.Buffer
	w := io.Writer(&buf1)

	_, err := io.Copy(w, f)
	assert.Nil(t, err)

	tMd5 := md5.New()
	assert.Equal(t, "0de2df12d07700d0e6582e84f27a7d5e", rcrypto.GetSumHash(tMd5, bytes.NewReader(buf1.Bytes())))

	tSha1 := sha1.New()
	f, _ = appFS.Open("/recalbox/share/roms/nes/file1")
	assert.Equal(t, "1faa34518f2cf179c2b50d36a44413506fa0398f", rcrypto.GetSumHash(tSha1, bytes.NewReader(buf1.Bytes())))

	tCrc := crc32.NewIEEE()
	f, _ = appFS.Open("/recalbox/share/roms/nes/file1")
	assert.Equal(t, "f2993d7e", rcrypto.GetSumHash(tCrc, bytes.NewReader(buf1.Bytes())))

}
